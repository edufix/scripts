import requests, csv, json, random, string

schoolId = "ipsindirapuramncr"
at = "0fUC0fzAznpRUDJrTt2gqlCljma9w3R4AIHYZG7F8kULFFwwJzPyEIuL6dCQkE7T"

host = "http://0.0.0.0:51657"

schoolDir = "dump/Indirapuram_Public_School"
studDir = schoolDir+"/pwd/studentPwd.csv"
tchDir = schoolDir+"/pwd/teacherPwd.csv"

headers = {
    'content-type': "application/json",
    'authorization': at,
    'cache-control': "no-cache"
    }

def genPassword():
    digits = "".join( [random.choice(string.digits) for i in xrange(8)] )
    chars = "".join( [random.choice(string.letters) for i in xrange(15)] )
    return digits + chars

def createProfile(url, dir_):
    with open(dir_, 'r') as f:
        reader = csv.reader(f)

        for row in reader:
            data = {
                "username": row[0],
                "name": row[1],
                "password": row[2],
                "email": "loopback"+genPassword()+"@loopback.com",
                "emailVerified": "true"
            }

            try:
                response = requests.post(url, headers=headers, data=json.dumps(data))
                print "[SUCCESS]", response.status_code, response.text
            except Exception as e:
                print "[FAILED] ", e

if __name__ == '__main__':
    createProfile(host+"/api/schools/"+schoolId+"/students", studDir)
    createProfile(host+"/api/schools/"+schoolId+"/teachers", tchDir)


# {
#   "name": "Indirapuram Public School,Ghaziabad",
#   "username": "ipsindirapuramncr",
#   "address": " 6, Nyaya Khand-I, Indirapuram,Ghaziabad (NCR); India",
#   "phone": [
#     "0120-3264427", "0-8882024545"
#   ],
#   "words": {
# "by": "Vishal Singh", "body": "Having started Indirapuram Public School in 2003, our Educational Society has consistently displayed full awareness of its social responsibilities through the various educational institutions set up in different parts of the country. "
# },
#   "email": "principal@ipsindirapuramncr.com",
#   "emailVerified": true
# }
