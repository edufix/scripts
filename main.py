import requests
import random, string, csv, re, json

'''
    Tune the PARAMETERS below only
'''
schoolId = "mrvncr"
at = "OcK2THnbKfYkSX3CkUGq6DBSRmgpXABeXo1o7Z3u1pzsGkabqgcGZuU1cDCobgvl"

host = "http://0.0.0.0:51657"

schoolDir = "dump/MRV_Public_School"
studDir = schoolDir + "/student.csv"
teacherDir = schoolDir + "/teacher.csv"

studPwdDir = schoolDir + "/studentPwd.csv"
teacherPwdDir = schoolDir + "/teacherPwd.csv"

colIndStud = [5, 1] # Student Coloumn Index for Adm. No. and Name respectively
stuAdmCleanRe = r'/' # Cleanup Regex for Admission Number
stuAdmReplaceRe = r'-'

colIndTch = [0, 1] # Teacher Coloumn Index for Adm. No. and Name respectively
tchAdmCleanRe = r'' # Cleanup Regex for Admission Number
tchAdmReplaceRe = r''
# END OF PARAMETERS

def genPassword():
    digits = "".join( [random.choice(string.digits) for i in xrange(8)] )
    chars = "".join( [random.choice(string.letters) for i in xrange(15)] )
    return digits + chars

def genUsername(row, type="student"):
    if type == "student":
        colInd = colIndStud
        admCleanRe = stuAdmCleanRe
        admReplaceRe = stuAdmReplaceRe

    else:
        colInd = colIndTch
        admCleanRe = tchAdmCleanRe
        admReplaceRe = tchAdmReplaceRe

    admNo = re.sub(admCleanRe, admReplaceRe, row[colInd[0]])
    name = re.sub(r'^(Dr|Mrs?|Ms)\.|( )', '', row[colInd[1]], flags=re.IGNORECASE)
    return schoolId+name+admNo

if __name__ == '__main__':
    url = host+"/api/schools/"+schoolId+"/students"

    headers = {
        'content-type': "application/json",
        'authorization': at,
        'cache-control': "no-cache"
        }

    with open(studPwdDir, 'w') as f:
        fieldnames = ['username', 'name', 'password']
        writer = csv.DictWriter(f, fieldnames=fieldnames)
        writer.writeheader()

        with open(studDir, 'rb') as f:
            reader = csv.reader(f)
            for row in reader:
                data = {
                    "username": genUsername(row),
                    "name": row[colIndStud[1]],
                    "password": genPassword(),
                    "email": "loopback"+genPassword()+"@loopback.com",
                    "emailVerified": "true"
                }

                try:
                    response = requests.post(url, headers=headers, data=json.dumps(data))
                    if response.status_code != 200:
                        raise NameError(data)
                    print "[SUCCESS] Student Modal ", response.status_code, response.text
                    writer.writerow({'username': data['username'], 'name': data['name'], 'password': data['password']})
                except Exception as e:
                    print "[FAILED] ", e



    # url = host+"/api/schools/"+schoolId+"/teachers"
    # with open(teacherPwdDir, 'w') as f:
    #     fieldnames = ['username', 'name', 'password']
    #     writer = csv.DictWriter(f, fieldnames=fieldnames)
    #     writer.writeheader()
    #
    #     with open(teacherDir, 'rb') as f:
    #         reader = csv.reader(f)
    #         for row in reader:
    #             data = {
    #                 "username": genUsername(row, type="teacher"),
    #                 "name": row[colIndTch[1]],
    #                 "password": genPassword(),
    #                 "email": "loopback"+genPassword()+"@loopback.com",
    #                 "emailVerified": "true"
    #             }
    #
    #             try:
    #                 response = requests.post(url, headers=headers, data=json.dumps(data))
    #                 if response.status_code != 200:
    #                     raise NameError(data)
    #                 print "[SUCCESS] Student Modal ", response.status_code, response.text
    #                 writer.writerow({'username': data['username'], 'name': data['name'], 'password': data['password']})
    #             except Exception as e:
    #                 print "[FAILED] ", e
